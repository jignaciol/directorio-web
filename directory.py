#!usr/bin/env python

# Proyecto: Directorio web hospital coromoto
# Version: 0.0.0
# Contribuyentes:
# Jose I. Luengo




import psycopg2
from bottle import route, run, Bottle, template, request, static_file

app = Bottle()

# Static Routes

@app.route('/static/<filename:re:.*\.css>')
def stylesheets(filename):
    return static_file(filename, root='static/css')

@app.route('/<filename:re:.*\.js>')
def javascripts(filename):
    return static_file(filename, root='static/js')

@app.route('/static/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
    return static_file(filename, root='static/img')

@app.route('/<filename:re:.*\.(eot|ttf|woff|svg)>')
def fonts(filename):
    return static_file(filename, root='static/fonts')



@app.route('/')
@app.route('/directorio')
@app.route('/directorio/page/<page:re:[0-9]+>', method='GET')
def actualizar_pagina(page=1):
     # Defino cadena de conexion
    conn_string = "host='10.121.6.4' port=5432 dbname='bdhcoromoto' user='admhc' password='shc21152115'"

    # Obtengo una conexion
    conn = psycopg2.connect(conn_string)
    c = conn.cursor()
    c.execute('select count(*) from agenda.agenda;')
    nrows = c.fetchall()
    nrows =  nrows[0][0]
    npages = nrows / 30
    offset = (int(page) * 45) - 45
    c.execute('SELECT nombre, telefono, departamento FROM agenda.agenda WHERE del = 0 and char_length(nombre) > 0 and char_length(departamento) > 0 ORDER BY departamento ASC LIMIT 45 OFFSET ' + str(offset) + ';')
    records = c.fetchall()
    c.close()

    return template('index', rows=records, npages=npages, apage=page)









run(app, host='localhost', port=9090, reloader=True, debug=True)
