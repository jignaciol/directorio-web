<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href='/static/font-awesome.min.css'>
    <link rel="stylesheet" href='/static/bootstrap.min.css'>
    <link rel='stylesheet' href='/static/board.css'>
    <link rel='stylesheet' href='/static/pagination.css'>
</head>
<body>
    <nav class='navbar navbar-default navbar-static-top'>
        <div class="container">
            <a href='#'class='navbar-brand'>Directorio</a>
        </div> <!-- end of container -->
    </nav>
    <br><br>

    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <!-- <div class="widget-header bordered-bottom bordered-themesecondary">
                        </div> -->
                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <div class="tickets-container">

                                    <ul class="tickets-list">
                                        <!-- Cabecera de la lista de usuarios del directorio -->
                                        <li class="ticket-item " style='background-color: #E3E3E3; font-size: 18px;'>
                                            <div class="row">
                                                <div class="ticket-user col-sm-5">
                                                    <span class="widget-caption themesecondary">Nombre</span>
                                                </div>
                                                <div class="ticket-time  col-sm-3">
                                                    <div class="divider hidden-md hidden-sm hidden-xs"></div>
                                                    <span class="widget-caption themesecondary">Numero</span>
                                                </div>
                                                <div class="ticket-type  col-sm-4">
                                                    <span class="divider hidden-xs"></span>
                                                    <span class="widget-caption themesecondary">Departamento</span>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- fin de cabecera -->
                                        %for row in rows:
                                            <li class="ticket-item">
                                                <div class="row">
                                                    <div class="ticket-user col-sm-5 ">
                                                        <img src="/static/user_1.jpg" class="user-avatar">
                                                        <span class="user-company text-uppercase">{{ row[0] }}</span>
                                                        <!-- <span class="user-at">at</span>
                                                        <span class="user-company">Microsoft</span> -->
                                                    </div>
                                                    <div class="ticket-type  col-sm-3">
                                                        <div class="divider hidden-xs"></div>
                                                        <!-- <i class="fa fa-clock-o"></i> -->
                                                        <span class="type">{{ row[1] }}</span>
                                                    </div>
                                                    <div class="ticket-type  col-sm-4">
                                                        <span class="divider hidden-xs"></span>
                                                        <span class="type">{{ row[2] }}</span>
                                                    </div>
                                                </div>
                                            </li>
                                        %end
                                    </ul>
                                    <div class='text-center'>
                                        <ul class='pagination'>
                                            % for page in range(1, npages):
                                                % if page==apage:
                                                    <li class='disabled'><a href='#'>{{page}}</a></li>
                                                % else:
                                                    <li><a href='/directorio/page/{{page}}'>{{page}}</a></li>
                                                % end
                                            % end
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
